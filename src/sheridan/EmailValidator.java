package sheridan;

public class EmailValidator {

	/*
	 * How the pattern works:
	 * - Make sure the first character is not a number
	 * - Match a lowercase character 3 or more times for the account name, this character can be followed by
	 * 		an uppercase character or a number
	 * - Match a single @ after the account name
	 * - Match a lowercase character or a number 3 or more times for the domain name after the @, this character
	 * 		can be followed by an uppercase character
	 * - Match a single period after the domain name
	 * - Match a non-numeric alphabetical character 2 or more times after the period
	 * - Make sure that this match can only be found once in the String by defining the start and end of the line
	 */
	private static final String EMAIL_REGEX = "^(?![0-9])([a-z][A-Z]*[0-9]*){3,}@(([a-z]|[0-9])[A-Z]*){3,}\\.[a-zA-z]{2,}$";
	
	public static boolean isValidEmail(String email) {
		boolean isValidEmail = false;
		if (email != null) {
			isValidEmail = email.matches(EMAIL_REGEX);
		}
		return isValidEmail;
	}
	
}
