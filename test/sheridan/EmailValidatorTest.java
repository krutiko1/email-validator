package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	@Test
	public void testIsEmailValidFormatReqRegular() {
		String email = "email@hello.world";
		assertTrue("Email format was not correct but was still considered correct",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidFormatReqBoundaryIn() {
		String email = "hello@email.world";
		assertTrue("Email format was not correct but was still considered correct",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidFormatReqStartSpaceBoundaryOut() {
		String email = " email@helloworld";
		assertFalse("Email starts with a space but was still considered correct",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidFormatReqNoPeriodBoundaryOut() {
		String email = "email@helloworld";
		assertFalse("Email has no period but was still considered correct",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidFormatReqAtAfterPeriodBoundaryOut() {
		String email = "email.hello@world";
		assertFalse("Email has a period before the @ but was still considered correct",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidFormatReqNoAtNoPeriodBoundaryOut() {
		String email = "emailhelloworld";
		assertFalse("Email has no period and no @ and was still considered valid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidFormatReqNullException() {
		String email = null;
		assertFalse("Email was null but was still considered correct",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidFormatReqEmptyException() {
		String email = "";
		assertFalse("Email was of length 0 but was still considered correct",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidAtReqNoAtBoundaryOut() {
		String email = "emailhello.world";
		assertFalse("Email has no '@' and was still considered valid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidAtReqMultipleAtBoundaryOut() {
		String email = "email@@hello.world";
		assertFalse("Email has multiple '@' and was still considered valid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidAccountReqRegular() {
		String email = "accountHello123@hello.world";
		assertTrue("Account section of email was correct and was still considered invalid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidAccountReqBoundaryIn() {
		String email = "aCcc@hello.world";
		assertTrue("Account section of email was correct and was still considered invalid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidAccountReqTooShortBoundaryOut() {
		String email = "cc@hello.world";
		assertFalse("Account section of email too short and was still considered valid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidAccountReqStartWithNumBoundaryOut() {
		String email = "1acc@hello.world";
		assertFalse("Account section of email starts with a number and was still considered valid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidAccountReqNotLowerCaseBoundaryOut() {
		String email = "ACCOUNT@hello.world";
		assertFalse("Account section of email is all upper case and was still considered valid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidAccountReqException() {
		String email = "@hello.world";
		assertFalse("Email has no account section and was still considered valid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidDomainReqRegular() {
		String email = "account@domain2.world";
		assertTrue("Domain section of email was correct and was still considered invalid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidDomainReqBoundaryIn() {
		String email = "account@d0m.world";
		assertTrue("Domain section of email was correct and was still considered invalid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidDomainReqTooShortBoundaryOut() {
		String email = "account@m1.world";
		assertFalse("Domain section of email too short and was still considered valid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidDomainReqNoLowerCaseOrNumberBoundaryOut() {
		String email = "account@DOMAIN.world";
		assertFalse("Domain section of email is all upper case and was still considered valid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidDomainReqException() {
		String email = "account@.world";
		assertFalse("Email has no domain section and was still considered valid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidExtensionReqRegular() {
		String email = "account@domain2.world";
		assertTrue("Extension section of email was correct and was still considered invalid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidExtensionReqBoundaryIn() {
		String email = "account@domain2.Wo";
		assertTrue("Extension section of email was correct and was still considered invalid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidExtensionReqTooShortBoundaryOut() {
		String email = "account@domain2.o";
		assertFalse("Extension section of email was too short and was still considered invalid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidExtensionReqOnlyNumericBoundaryOut() {
		String email = "account@domain2.123";
		assertFalse("Extension section of email was only numeric and was still considered valid",
				EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testIsEmailValidExtensionReqException() {
		String email = "account@domain2.";
		assertFalse("Email has no extension section and was still considered valid",
				EmailValidator.isValidEmail(email));
	}
	

}
